package com.paascloud.provider.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

import javax.servlet.http.HttpServletResponse;

/**
 * The class Resource server config.
 *
 * @author paascloud.net @gmail.com
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	@Value("${paascloud.auth.enable:true}")
	private boolean enableAuth;

	@Override
	public void configure(HttpSecurity http) throws Exception {

		String[] antPatterns = new String[]{"/pay/alipayCallback","/druid/**", "/swagger-ui.html", "/swagger-resources/**", "/v2/api-docs", "/api/applications"};
		if(!enableAuth){
			antPatterns = new String[]{"/**"};
		}
		http
				.headers().frameOptions().disable()
				.and()
				.csrf().disable()
				.exceptionHandling()
				.authenticationEntryPoint((request, response, authException) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED))
				.and()
				.authorizeRequests().antMatchers(antPatterns).permitAll()
				.anyRequest().authenticated();
	}
}
